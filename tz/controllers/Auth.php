<?php

include 'config.php';
include('../classes/users.php');

$data = new Users();
$error = array();
// login process 
function loginNow($user)
{
    $_SESSION['idU'] = $user['id'];
    $_SESSION['username'] = $user['username'];
    $_SESSION['msg'] = 'you are logged in';
    header('location:profile.php');
    exit();
}


//signning up
if (isset($_POST['signup'])) {
    $username = htmlspecialchars($_POST['username']);
    $email = htmlspecialchars($_POST['useremail']);
    $password = $_POST['password'];
    $confpassword = $_POST['confpassword'];
    $picture = $_FILES['picture'];
    $status = $_POST['status'];
    //validation
    if (empty($username)) {
        $error['username'] = "Username required";
    }
    if (empty($email)) {
        $error['useremail'] = "Email required";
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error['useremail'] = "Email adress is not valid";
    }
    if ($password !== $confpassword) {
        $error['password'] = "The two passwords don't match";
    }
    if (empty($password)) {
        $error['password'] = "Password required";
    }
    if (empty($picture)) {
        $error['picture'] = "Picture required";
    }
    $password = password_hash($_POST['password'], PASSWORD_DEFAULT);
    $con = $data->connect();
    $emailQuery = "SELECT * FROM users WHERE useremail = ? LIMIT 1";
    $stm = $con->prepare($emailQuery);
    $stm->bind_param('s', $email);
    $stm->execute();
    $result = $stm->get_result();
    $usercount = $result->num_rows;
    if ($usercount > 0) {
        $error['useremail'] = "useremail already exists";
    }
    //checking if the number of error is 0
    if (count($error) === 0) {
        $picture = $_FILES['picture']['name'];
        $upload = "uploads/" . $picture;
        //storing pictures to the uploads file
        move_uploaded_file($_FILES['picture']['tmp_name'], $upload);

        $push = $data->sign_up($username, $email, $password, $picture, $status);
        loginNow($push);
    }
}

// logging in 
if (isset($_POST['login'])) {

    $useremail = htmlspecialchars($_POST['useremail']);
    $password = $_POST['password'];

    //validation
    if (empty($useremail)) {
        $error['useremail'] = "useremail required";
    }
    if (empty($password)) {
        $error['password'] = "password required";
    }

    if (count($error) === 0) {
        $user = $data->userLogin('users', ['useremail' => $_POST['useremail']]);
        $admin = $data->userLogin('admins', ['email' => $_POST['useremail']]);

        if ($user && password_verify($password, $user['password'])) {
            //login success
            $_SESSION['Id'] = $user['Id'];
            $_SESSION['username'] = $user['username'];
            $_SESSION['useremail'] = $user['useremail'];
            $_SESSION['picture'] = $user['picture'];
            $_SESSION['status'] = $user['status'];
            header("Location:profile.php");
            exit();
        }
        if ($admin && password_verify($password, $admin['password'])) {
            //login success
            $_SESSION['Id'] = $admin['Id'];
            $_SESSION['admin_name'] = $admin['admin_name'];
            $_SESSION['email'] = $admin['email'];
            $_SESSION['picture'] = $admin['picture'];
            header("Location:admin_panel.php");
            exit();
        } else {
            $error['login_fail'] = "wrong credition";
        }
    }
}
