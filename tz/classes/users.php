<?php
session_start();

class Users extends DB
{
    public function sign_up($username, $email, $password, $upload, $status)
    {
        $sql2 = "INSERT INTO `users`(username,useremail,password,picture,status) VALUES('$username','$email','$password','$upload','$status')";
        $result = $this->connect()->query($sql2);
        return $result;
    }


    public function userLogin($table, $condition)
    {
        $conn = $this->connect();
        $sql = "SELECT * FROM $table";
        $i = 0;
        foreach ($condition as $key => $value) {
            if ($i === 0) {
                $sql = $sql . " WHERE $key=?";
            } else {
                $sql = $sql . " AND $key=?";
            }
            $i++;
        }
        $sql = $sql . " LIMIT 1";

        $stm = $conn->prepare($sql);
        $value = array_values($condition);
        $type = str_repeat('s', count($value));
        $stm->bind_param($type, ...$value);
        $stm->execute();
        $result = $stm->get_result()->fetch_assoc();
        return $result;
    }

    public function adminLogin($table, $data)
    {
        $con = $this->connect();
        $sql = "SELECT * FROM $table WHERE email = ? LIMIT 1 ";
        $stm = $con->prepare($sql);
        $stm->bind_param('s', $data);
        $stm->execute();
        $result = $stm->get_result();
        $admin = $result->fetch_assoc();
        return $admin;
    }
}
